/*
 * char_to_keystroke.h
 *
 *
 */

#ifndef CHAR_TO_KEYSTROKE
#define CHAR_TO_KEYSTROKE

#include "USB_KB_Device.h"
#include "usb_device.h"


#define SHORT_DELAY 3
#define MEDIUM_DELAY 30
#define LONG_DELAY 60
#define LAUNCH_DELAY 200
/*
 * Variables
 */
TM_USB_HIDDEVICE_Keyboard_t kb;
char                        keyToUpper;

TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_INT_Status;

void charToKeystroke(char keyIn, TM_USB_HIDDEVICE_Keyboard_t * kb);



TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardStructInit(TM_USB_HIDDEVICE_Keyboard_t* Keyboard_Data);
TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardSend(TM_USB_HIDDEVICE_Keyboard_t* Keyboard_Data);
TM_USB_HIDDEVICE_Status_t TM_USB_HIDDEVICE_KeyboardReleaseAll(void);

void ExecPayloadDelivery();

#endif
