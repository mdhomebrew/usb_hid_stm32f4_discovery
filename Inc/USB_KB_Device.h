/*
 *  USB Keyboard Device types
 *  Shamelessly stolen from https://stm32f4-discovery.net/2014/09/library-34-stm32f4-usb-hid-device/
 *  and modified for my purposes.
 *  Thank you tilz0R for doing such an amazing job setting up USB HID for the STM32F4 series.
 */


#ifndef __USB_KB_DEVICE_H
#define __USB_KB_DEVICE_H

#include "stm32f4xx_hal.h"

#define USB_KEYBOARD 0x01

enum USB_KB_CODES {
    kb_0x00 = 0x00, // Reserved (no event indicated)
	kb_0x01 = 0x01, // Keyboard ErrorRollOver
	kb_0x02 = 0x02, // Keyboard POSTFail
	kb_0x03 = 0x03, // Keyboard ErrorUndefined
	kb_a = 0x04	, // Keyboard a and A
	kb_b = 0x05	, // Keyboard b and B
	kb_c = 0x06	, // Keyboard c and C
	kb_d = 0x07	, // Keyboard d and D
	kb_e = 0x08	, // Keyboard e and E
	kb_f = 0x09	, // Keyboard f and F
	kb_g = 0x0A	, // Keyboard g and G
	kb_h = 0x0B	, // Keyboard h and H
	kb_i = 0x0C	, // Keyboard i and I
	kb_j = 0x0D	, // Keyboard j and J
	kb_k = 0x0E	, // Keyboard k and K
	kb_l = 0x0F	, // Keyboard l and L
	kb_m = 0x10	, // Keyboard m and M
	kb_n = 0x11	, // Keyboard n and N
	kb_o = 0x12	, // Keyboard o and O
	kb_p = 0x13	, // Keyboard p and P
	kb_q = 0x14	, // Keyboard q and Q
	kb_r = 0x15	, // Keyboard r and R
	kb_s = 0x16	, // Keyboard s and S
	kb_t = 0x17	, // Keyboard t and T
	kb_u = 0x18	, // Keyboard u and U
	kb_v = 0x19	, // Keyboard v and V
	kb_w = 0x1A	, // Keyboard w and W
	kb_x = 0x1B	, // Keyboard x and X
	kb_y = 0x1C	, // Keyboard y and Y
	kb_z = 0x1D	, // Keyboard z and Z
	kb_1 = 0x1E	, // Keyboard 1 and !
	kb_2 = 0x1F	, // Keyboard 2 and @
	kb_3 = 0x20	, // Keyboard 3 and #
	kb_4 = 0x21	, // Keyboard 4 and $
	kb_5 = 0x22	, // Keyboard 5 and %
	kb_6 = 0x23	, // Keyboard 6 and ^
	kb_7 = 0x24	, // Keyboard 7 and &
	kb_8 = 0x25	, // Keyboard 8 and *
	kb_9 = 0x26	, // Keyboard 9 and (
	kb_0 = 0x27	, // Keyboard 0 and )
	kb_enter = 0x28	, // Keyboard Return (ENTER)
	kb_escape = 0x29	, // Keyboard ESCAPE
	kb_backspace = 0x2A	, // Keyboard DELETE (Backspace)
	kb_tab = 0x2B	, // Keyboard Tab
	kb_space = 0x2C	, // Keyboard Spacebar
	kb_dash = 0x2D	, // Keyboard - and (underscore)
	kb_equals = 0x2E	, // Keyboard = and +
	kb_leftbracket = 0x2F	, // Keyboard [ and {
	kb_rightbracket = 0x30	, // Keyboard ] and }
	kb_slashpipe = 0x31	, // Keyboard \ and |
	kb_poundtilde = 0x32	, // Keyboard Non-US # and ~
	kb_colon = 0x33	, // Keyboard ; and :
	kb_quote = 0x34	, // Keyboard ' and "
	kb_tilde = 0x35	, // Keyboard Grave Accent and Tilde
	kb_comma = 0x36	, // Keyboard, and <
	kb_period = 0x37	, // Keyboard . and >
	kb_slashquestion = 0x38	, // Keyboard / and ?
	kb_CAPSLOCK = 0x39	, // Keyboard Caps Lock
	kb_f1 = 0x3A	, // Keyboard F1
	kb_f2 = 0x3B	, // Keyboard F2
	kb_f3 = 0x3C	, // Keyboard F3
	kb_f4 = 0x3D	, // Keyboard F4
	kb_f5 = 0x3E	, // Keyboard F5
	kb_f6 = 0x3F	, // Keyboard F6
	kb_f7 = 0x40	, // Keyboard F7
	kb_f8 = 0x41	, // Keyboard F8
	kb_f9 = 0x42	, // Keyboard F9
	kb_f10 = 0x43	, // Keyboard F10
	kb_f11 = 0x44	, // Keyboard F11
	kb_f12 = 0x45	, // Keyboard F12
	kb_prntscrn = 0x46	, // Keyboard PrintScreen
	kb_scrllk = 0x47	, // Keyboard Scroll Lock
	kb_pause = 0x48	, // Keyboard Pause
	kb_insert = 0x49	, // Keyboard Insert
	kb_home = 0x4a	, // Keyboard Home
	kb_pageup = 0x4b	, // Keyboard PageUp
	kb_delete = 0x4c	, // Keyboard Delete Forward
	kb_end = 0x4d,  // Keyboard End
	kb_pagedown = 0x4e	, // Keyboard PageDown
	kb_right = 0x4f	, // Keyboard RightArrow
	kb_left = 0x50	, // Keyboard LeftArrow
	kb_down = 0x51	, // Keyboard DownArrow
	kb_up = 0x52	, // Keyboard UpArrow
	} KB_Codes_t;
	/*
	 * Didn't need the rest of these... yet. Easy to enable.
	 *
	kb_ = 0x53	Keypad Num Lock and Clear
	kb_ = 0x54	Keypad /
	kb_ = 0x55	Keypad *
	kb_ = 0x56	Keypad -
	kb_ = 0x57	Keypad +
	kb_ = 0x58	Keypad ENTER
	kb_ = 0x59	Keypad 1 and End
	kb_ = 0x5A	Keypad 2 and Down Arrow
	kb_ = 0x5B	Keypad 3 and PageDn
	kb_ = 0x5C	Keypad 4 and Left Arrow
	kb_ = 0x5D	Keypad 5
	kb_ = 0x5E	Keypad 6 and Right Arrow
	kb_ = 0x5F	Keypad 7 and Home
	kb_ = 0x60	Keypad 8 and Up Arrow
	kb_ = 0x61	Keypad 9 and PageUp
	kb_ = 0x62	Keypad 0 and Insert
	kb_ = 0x63	Keypad . and Delete
	kb_ = 0x64	, // Keyboard Non-US \ and |
	kb_ = 0x65	, // Keyboard Application
	kb_ = 0x66	, // Keyboard Power
	kb_ = 0x67	Keypad =
	kb_ = 0x68	, // Keyboard F13
	kb_ = 0x69	, // Keyboard F14
	kb_ = 0x6A	, // Keyboard F15
	kb_ = 0x6B	, // Keyboard F16
	kb_ = 0x6C	, // Keyboard F17
	kb_ = 0x6D	, // Keyboard F18
	kb_ = 0x6E	, // Keyboard F19
	kb_ = 0x6F	, // Keyboard F20
	kb_ = 0x70	, // Keyboard F21
	kb_ = 0x71	, // Keyboard F22
	kb_ = 0x72	, // Keyboard F23
	kb_ = 0x73	, // Keyboard F24
	kb_ = 0x74	, // Keyboard Execute
	kb_ = 0x75	, // Keyboard Help
	kb_ = 0x76	, // Keyboard Menu
	kb_ = 0x77	, // Keyboard Select
	kb_ = 0x78	, // Keyboard Stop
	kb_ = 0x79	, // Keyboard Again
	kb_ = 0x7A	, // Keyboard Undo
	kb_ = 0x7B	, // Keyboard Cut
	kb_ = 0x7C	, // Keyboard Copy
	kb_ = 0x7D	, // Keyboard Paste
	kb_ = 0x7E	, // Keyboard Find
	kb_ = 0x7F	, // Keyboard Mute
	kb_ = 0x80	, // Keyboard Volume Up
	kb_ = 0x81	, // Keyboard Volume Down
	kb_ = 0x82	, // Keyboard Locking Caps Lock
	kb_ = 0x83	, // Keyboard Locking Num Lock
	kb_ = 0x84	, // Keyboard Locking Scroll Lock
	kb_ = 0x85	Keypad Comma
	kb_ = 0x86	Keypad Equal Sign
	kb_ = 0x87	, // Keyboard International1
	kb_ = 0x88	, // Keyboard International2
	kb_ = 0x89	, // Keyboard International3
	kb_ = 0x8A	, // Keyboard International4
	kb_ = 0x8B	, // Keyboard International5
	kb_ = 0x8C	, // Keyboard International6
	kb_ = 0x8D	, // Keyboard International7
	kb_ = 0x8E	, // Keyboard International8
	kb_ = 0x8F	, // Keyboard International9
	kb_ = 0x90	, // Keyboard LANG1
	kb_ = 0x91	, // Keyboard LANG2
	kb_ = 0x92	, // Keyboard LANG3
	kb_ = 0x93	, // Keyboard LANG4
	kb_ = 0x94	, // Keyboard LANG5
	kb_ = 0x95	, // Keyboard LANG6
	kb_ = 0x96	, // Keyboard LANG7
	kb_ = 0x97	, // Keyboard LANG8
	kb_ = 0x98	, // Keyboard LANG9
	kb_ = 0x99	, // Keyboard Alternate Erase
	kb_ = 0x9A	, // Keyboard SysReq/Attention
	kb_ = 0x9B	, // Keyboard Cancel
	kb_ = 0x9C	, // Keyboard Clear
	kb_ = 0x9D	, // Keyboard Prior
	kb_ = 0x9E	, // Keyboard Return
	kb_ = 0x9F	, // Keyboard Separator
	kb_ = 0xA0	, // Keyboard Out
	kb_ = 0xA1	, // Keyboard Oper
	kb_ = 0xA2	, // Keyboard Clear/Again
	kb_ = 0xA3	, // Keyboard CrSel/Props
	kb_ = 0xA4	, // Keyboard ExSel
	kb_ = 0xE0	, // Keyboard LeftControl
	kb_ = 0xE1	, // Keyboard LeftShift
	kb_ = 0xE2	, // Keyboard LeftAlt
	kb_ = 0xE3	, // Keyboard Left GUI
	kb_ = 0xE4	, // Keyboard RightControl
	kb_ = 0xE5	, // Keyboard RightShift
	kb_ = 0xE6	, // Keyboard RightAlt
	kb_ = 0xE7	, // Keyboard Right GUI
 */


typedef enum {
	TM_USB_HIDDEVICE_Status_LibraryNotInitialized = 0x00, /*!< Library is not initialized yet */
	TM_USB_HIDDEVICE_Status_Connected,                    /*!< Device is connected and ready to use */
	TM_USB_HIDDEVICE_Status_Disconnected,                 /*!< Device is not connected */
	TM_USB_HIDDEVICE_Status_IdleMode,                     /*!< Device is is IDLE mode */
	TM_USB_HIDDEVICE_Status_SuspendMode                   /*!< Device is in suspend mode */
} TM_USB_HIDDEVICE_Status_t;

typedef enum {
	TM_USB_HIDDEVICE_Button_Released = 0x00, /*!< Button is not pressed */
	TM_USB_HIDDEVICE_Button_Pressed = 0x01   /*!< Button is pressed */
} TM_USB_HIDDEVICE_Button_t;



typedef struct {
	TM_USB_HIDDEVICE_Button_t L_CTRL;  /*!< Left CTRL button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t L_ALT;   /*!< Left ALT button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t L_SHIFT; /*!< Left SHIFT button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t L_GUI;   /*!< Left GUI (Win) button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t R_CTRL;  /*!< Right CTRL button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t R_ALT;   /*!< Right ALT button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t R_SHIFT; /*!< Right SHIFT button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	TM_USB_HIDDEVICE_Button_t R_GUI;   /*!< Right GUI (Win) button. This parameter can be a value of @ref TM_USB_HIDDEVICE_Button_t enumeration */
	uint8_t Key1;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
	uint8_t Key2;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
	uint8_t Key3;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
	uint8_t Key4;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
	uint8_t Key5;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
	uint8_t Key6;                      /*!< Key used with keyboard. This can be whatever. Like numbers, letters, everything. */
} TM_USB_HIDDEVICE_Keyboard_t;

#endif
