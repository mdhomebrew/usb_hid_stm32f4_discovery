# README #

This project builds a USB Keyboard/Mouse HID for the STM32F4 Discovery board.

The application registers as a HID and, using keyboard/mouse commands, types out, saves, and executes a game cheat for Chocolate DOOM. 

This is useful as a testbed for different payloads for the keystroke delivery system.

The Chocolate DOOM game cheat uses keyboard shortcuts to enable different options:

* Alt + +: increase armor
* Alt + 0: unlimited ammo
* Alt + [number]: unlock weapon tied to that slot - allows access to the BFG, Plasma Cannon, etc on the first level of DOOM for all you noobs who are struggling with it.

### What is this repository for? ###

* USB Keyboard keystroke payload delivery designed for the STM32F4 Discovery board
* Delivers and executes a game cheat for the original DOOM.

### Tools needed ###

* http://www.openstm32.org/System+Workbench+for+STM32
* http://www.st.com/en/embedded-software/stm32cube-embedded-software.html